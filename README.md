This is the Earthdawn system for Foundry VTT. The game system is currently under very active development, but please see the [wiki](https://gitlab.com/fattom23/earthdawn4e/-/wikis/home) page for current documentation and the [Issues](https://gitlab.com/fattom23/earthdawn4e/-/issues) section to report or track status of bugs and feature requests. 


