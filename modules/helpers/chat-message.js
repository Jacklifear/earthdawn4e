//###########################################################
/** Chat message functions */
/* currently implemented chat functions
- Active Mods
- Rule of One
- Information on used Karma
- Information on used Devotions
- Final Step
- Extra Successes
- Success or failure of roll
- "Roll Damage/Effect" - Button
- Apply Effect - Button
*/
/* chat message functions to be implemented
- Counter Physical Attack - Button leading to a Popup with potential choices
- Counter Spell Attack - Button leading to a Popup with potential choices (are there more than Iron Will?)
- Counter Social Attack - Button leading to a Popup with potential choices (are there more than Resist Taunt?)
- enhance "Roll Damage/Effect" Popup by including Damage adder of Charakter, like Thunderslam
*/
//###########################################################
export async function chatOutput(input, roll) {
  console.log('[EARTHDAWN] Chat Input', input);

  let rollMode = game.settings.get('core', 'rollMode');

  //###########################################################
  /** message types  */
  //###########################################################
  const title = `${input.name} ${game.i18n.localize('earthdawn.r.rolls')} ${input.talent}!`;
  const blind = rollMode === 'blindroll';
  const whisper = ['gmroll', 'blindroll'].includes(rollMode) ? ChatMessage.getWhisperRecipients('GM') : [];
  const speaker = ChatMessage.getSpeaker({ alias: name });

  //###########################################################
  /** Flavor constructor  */
  //###########################################################
  const flavor = title + getKarma(input) +  getDevotion(input) + getRoll(input) + await getActiveMods(input) + getSuccess(input) + getRuleOfOne(roll) + getOutcome(input) + getAttack(input) + getEffects(input);
  await roll.toMessage({ flavor, blind, whisper, speaker });
}
//###########################################################
/** Active Mods */
/*  active Mods will be all modifiers affecting rolls.*/
//###########################################################
async function getActiveMods(input){
  const template = 'systems/earthdawn4e/templates/chat/activeMods.hbs';
  const templateData = {};
  templateData['messageData'] = input;
  return await renderTemplate(template, templateData);
}

//###########################################################
/** Rule of One */
/*  if two or more dice show a one --> Failure */
//###########################################################
function getRuleOfOne(roll) {
  let Ones = 0;
  let notOnes = 0;
  roll.dice.forEach((set) => {
    set.results.forEach((dieResult) => {
      if (dieResult.result === 1) Ones++;
    });
  });
  roll.dice.forEach((set) => {
    set.results.forEach((dieResult) => {
      if (dieResult.result !== 1) notOnes++;
    });
  });

  let onlyOnes = game.i18n.localize('earthdawn.o.onlyOnes');
  return Ones > 1 && notOnes === 0 ? `<div class="rule-of-one">${onlyOnes}</div>` : '';
}

//###########################################################
/** Used Karma  */
//###########################################################
function getKarma(input) {
  return input.karmaUsed > 0
    ? `<div>
            ${game.i18n.localize('earthdawn.y.youUsed')} 
            ${input.karmaUsed} ${game.i18n.localize('earthdawn.k.karma')}
         </div>`
    : '';
}

//###########################################################
/** Used Devotioin  */
//###########################################################
function getDevotion(input) {
  return input.devotionUsed > 0
    ? `<div>
          ${game.i18n.localize('earthdawn.y.youUsed')} 
          ${input.devotionUsed} ${game.i18n.localize('earthdawn.d.devotion')}
      </div>`
    : '';
}

//###########################################################
/** Final Step  */
//###########################################################
function getRoll(input) {
  return `
        <div>
            <span><a class="show-hidden"><i class="fas fa-sort"></i></a> ${game.i18n.localize('earthdawn.s.step')}: ${input.finalstep}</span>
        </div>`;
}

//###########################################################
/** Successes  */
//###########################################################
function getSuccess(input) {
  return input.extraSuccess
    ? `<div class="extra-success">
            ${game.i18n.localize('earthdawn.y.youGot')} 
            ${input.extraSuccess} 
            ${game.i18n.localize('earthdawn.s.successExtra')}
        </div>`
    : '';
}

//###########################################################
/** Success or Failure  */
//###########################################################
function getOutcome(input) {
  if ('margin' in input) {
    return input.margin >= 0
      ? `<div class="extra-success">${game.i18n.localize('earthdawn.s.success')}!</div>`
      : `<div class="rule-of-one">${game.i18n.localize('earthdawn.f.failure')}!</div>`;
  } else {
    return '';
  }
}

//###########################################################
/** "Damage" pop up */
// to be separated like Option box
//###########################################################
function getAttack(input) {
  let attackSection = '';
  if (input.rolltype === 'attack') {
    if (input.itemtype === 'attack') {
      attackSection = `<div>
                    <a class='damageRollNPC effectTest' data-actorId='${input.actorId}' data-damageStep='${
        input.damage
      }' data-extraSuccess='${input.extraSuccess}'>
                        ${game.i18n.localize('earthdawn.r.rollDamage')}
                        <i class='effectTest fas fa-dice'></i>
                    </a>
                </div>`;
    } else {
      attackSection = `<div>
                    <a class='damageRoll effectTest' data-actorId='${input.actorId}' data-weaponId='${input.weaponId}' data-damageBonus='${
        input.damageBonus
      }'data-extraSuccess='${input.extraSuccess}'>
                        ${game.i18n.localize('earthdawn.r.rollDamage')}
                        <i class='effectTest fas fa-dice'></i>
                    </a>
                </div>`;
    }
  } else if (input.rolltype === 'weapondamage' || input.rolltype === 'attackdamage' || input.rolltype === 'weapondamageRanged' || input.rolltype === 'spelldamage') {
    attackSection = `<div>
                <a class='apply-damage' data-damageSource='${input.rolltype}' data-damageType='${input.damagetype}' data-damageTotal='${
      input.total
    }' >
                    ${game.i18n.localize('earthdawn.a.applyDamage')}
                    <i class='apply-damage fas fa-dice'></i>
                </a>
            </div>`;
  } else if (input.rolltype === 'spell') {
    attackSection = `<div>
                <a class='spellEffect effectTest' data-actorId='${input.actorId}' data-spellName='${input.spellName}' data-extraSuccess='${
      input.extraSuccess
    }'>
                    ${game.i18n.localize('earthdawn.e.effect')}
                    <i class='effectTest fas fa-dice'></i>
                </a>
       </div>`;
  }
  return attackSection;
}

//###########################################################
/** apply Effects  */
//###########################################################
function getEffects(input) {
  if (input.rolltype === 'spell') {
    let effectSection = '';
    //console.log(input.spellId);
    let actor = game.actors.get(input.actorId);
    let spell = actor.items.get(input.spellId);
    const targets = Array.from(game.user.targets);

    if (spell.hasOwnProperty('effects') && spell.effects.size > 0 && targets.length > 0) {
      effectSection += `<div><a class='applySpellEffect' data-actorId='${input.actorId}' data-spellId='${
        input.spellId
      }'>${game.i18n.localize('earthdawn.a.applyEffect')}</a>`;
    }
    return effectSection;
  } else {
    return '';
  }
}


