export async function resourceBox(inputs) {
    return await new Promise((resolve) => {
      new Dialog({
        title: "Resources",
        content: inputs.text,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
               
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });
  }