export function explodify(dice) {
  if (dice) {
    const regex = /(d[\d]+)/g;
    return dice.replace(regex, `$1x`);
  } else {
    return 0;
  }
}
