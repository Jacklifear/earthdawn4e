export function karmaAllowed(inputs) {
  let karma = 0;
  if (inputs.type === 'talent' && inputs.karmarequested === 'true') {
    karma = 1;
  }
  return karma;
}
