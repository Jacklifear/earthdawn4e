import { explodify } from './explodify.js';
import { getDice } from './get-dice.js';

export async function rollDiceChat() {
  //////////////////
  // Dialog: ask for step and karma/devotion
  /////////////////

  const label1 = game.i18n.localize('earthdawn.s.step');
  const label2 = game.i18n.localize('earthdawn.k.karma');
  const label3 = game.i18n.localize('earthdawn.d.devotion');
  const diceDialogHtml = `
    <div class="selectable-dice-popup">
      <div class="misc-mod">
        <label class="misc-mod-description">${label1}:</label>
        <input class="misc-mod-input circleInput" data-dtype="Number" id="stepNumber" /><br/>
      </div>
      <div class="misc-mod">
        <label class="misc-mod-description">${label2}:</label>
        <input class="misc-mod-input circleInput" data-dtype="Number" id="karmaDice" /><br/>
      </div>
      <div class="misc-mod">
        <label class="misc-mod-description">${label3}:</label>
        <input class="misc-mod-input circleInput" data-dtype="Number" id="devotionDice" />
      </div>
    </div> 
  `;
  let step = await new Promise((resolve) => {
    new Dialog({
      title: game.i18n.localize('earthdawn.r.rollStep'),
      content: diceDialogHtml,
      buttons: {
        ok: {
          label: game.i18n.localize('earthdawn.o.ok'),
          callback: (html) => {
            resolve({
              step: html.find('#stepNumber').val(),
              karma: html.find('#karmaDice').val(),
              devotion: html.find('#devotionDice').val(),
            });
          },
        },
      },
      default: 'ok',
    }).render(true);
  });

  let dice = getDice(step.step);

  //////////////////
  // Check for karma and devotion
  /////////////////

  if (step.karma > 0) {
    dice += `+ ${step.karma}d6`;
  }
  //devotion - I'm not sure how to geht this done. I believe the regex part will replace the dice value... where is the function for that?
  if (step.devotion > 0) {
    dice += `+ ${step.devotion}d4`;
  }

  //////////////////
  // Roll the dice
  /////////////////
  let roll = new Roll(explodify(dice));
  await roll.evaluate();

  console.log('[EARTHDAWN] Roll', roll);

  //////////////////
  // Display the result
  /////////////////
  await roll.toMessage();
}
