import { chatOutput } from '../helpers/chat-message.js';
import { explodify } from '../helpers/explodify.js';

export async function rollDice(actor, inputs) {
  //console.log(actor);
  console.log('[EARTHDAWN] Roll Inputs', inputs);

  //#######################################################################################
  /** Variables */
  //#######################################################################################

  let dice = actor.getDice(inputs.finalstep);
  inputs.karmaUsed = inputs.karma ? inputs.karma : 0;
  inputs.devotionUsed = inputs.devotion ? inputs.devotion : 0;
  let extraSuccess = 0;
  let karmaDie =
    actor.system.karmaDie === 'S8'
      ? '2d6'
      : actor.system.karmaDie === 'S9'
      ? 'd8+d6'
      : actor.system.karmaDie === 'S10'
      ? '2d8'
      : actor.system.karmaDie;
  let devotionDie = actor.system.devotionDie;

  //#######################################################################################
  /** Karma and Devotion usage */
  //#######################################################################################
  if (inputs.karmaUsed > 0 && inputs.devotionUsed == 0) {
    let karmaNew = actor.system.karma.value - inputs.karmaUsed;
    if (karmaNew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
      return false;
    }
    let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
    let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

    dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}`;
  } else if (inputs.devotionUsed > 0 && inputs.karmaUsed == 0) {
    let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
    if (actor.system.devotion.max > 0 && devotionNew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.d.devotionNo'));
      return false;
    }
    dice += `+` + inputs.devotionUsed + inputs.devotionDie;
  } else if (inputs.karmaUsed > 0 && inputs.devotionUsed > 0) {
    let karmaNew = actor.system.karma.value - inputs.karmaUsed;
    let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
    if (karmaNew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
      return false;
    }
    if (actor.system.devotion.max > 0 && devotionNew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.d.devotionNo'));
      return false;
    }
    let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
    let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

    dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}` + `+` + inputs.devotionUsed + inputs.devotionDie;
  }
  let formula = explodify(dice);
  let r = new Roll(`${formula}`);

  //#######################################################################################
  /** Roll evaluation */
  //#######################################################################################
  await r.evaluate();

  console.log('[EARTHDAWN] Roll', r);

  if (r.roll) {
    if (inputs.karmaUsed > 0) {
      let karmaNew = actor.system.karma.value - inputs.karmaUsed;
      await actor.update({ 'system.karma.value': karmaNew });
    }
    if (inputs.devotionUsed > 0) {
      let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
      await actor.update({ 'system.devotion.value': devotionNew });
    }

    if (inputs.strain > 0) {
      let damageNew = Number(actor.system.damage.value) + Number(inputs.strain);
      await actor.update({ 'system.damage.value': damageNew });
    }
  }

  //#######################################################################################
  /** Ammo Tracking - removing Ammo */
  //#######################################################################################
  if (inputs.ammoDecrement) {
    //console.log(inputs.ammoId);
    let ammoItem = actor.items.get(inputs.ammoId);
    //console.log(ammoItem.system.amount);
    let amountNew = Number(ammoItem.system.amount - 1);
    //console.log(amountNew);
    await ammoItem.update({ 'system.amount': amountNew });
  }

  //#######################################################################################
  /** Extra Successes */
  //#######################################################################################
  if (inputs.difficulty > 0) {
    let margin = r.total - inputs.difficulty;
    inputs.margin = margin;
    inputs.extraSuccess = margin > 0 ? Math.floor(margin / 5) : 0;
  } else {
    inputs.extraSuccess = 0;
  }


  //#######################################################################################
  /** INputs */
  //#######################################################################################
  inputs.dice = dice;
  inputs.name = actor.name;

  //#######################################################################################
  /** Initiative inputs */
  //#######################################################################################
  if (inputs.initiative === true) {
    actor.rollToInitiative(inputs, r);
  }

  //#######################################################################################
  /** Return & Chat*/
  //#######################################################################################
  chatOutput(inputs, r);
  return inputs;
}
