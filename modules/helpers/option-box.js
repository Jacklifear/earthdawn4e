import { getTargetDifficulty } from './combat-modifiers.js';

export async function getOptionBox(inputs) {
  return new Promise((resolve) => {
    let OptBox = new OptionBox(inputs, resolve);
    OptBox.render(true);
  });
}

export class OptionBox extends Application {
  constructor(inputs, resolve, tabs) {
    super({
      title: inputs.title ? inputs.title : game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
    });
    this.inputs = inputs;
    console.log('this inputs ' + this.inputs);
    this.activeMods = inputs.activeMods ? inputs.activeMods : [];
    this.resolve = resolve;
    this.tabs = tabs;

    this.currentTargetDefenseType = inputs.defenseTarget;
    this.currentGroupDefenseType = inputs.defenseGroup;
    this.currentTarget = game.user.targets.first()?.actor;
    this.currentTargets = [...game.user.targets.map((t) => t.actor)];
    console.log('currentTargets: ' + this.currentTargets);
    this.numTargets = game.user.targets.size;
    this.difficultyBoxElementID = '#difficulty_box';
    this.targetChangedHookID = Hooks.on('targetToken', this._onTargetChanged.bind(this));

    this._updateDifficulty(inputs.difficulty);
  }

  activateListeners(html) {
    html.find("[data-button='cancel']").click((ev) => {
      return this._onCancel();
    });

    html.find("[data-button='ok']").click((ev) => {
      return this._onOK();
    });

    html.find('.difficulties .defenseTarget input[type=radio]').click((ev) => {
      this._onChangeTargetDefense(ev);
    });

    html.find('.difficulties .defenseGroup input[type=radio]').click((ev) => {
      this._onChangeGroupDefense(ev);
    });
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['optionBox', 'dialog'],
      template: `systems/earthdawn4e/templates/popups/option-box.hbs`,
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'inputfields',
        },
      ],
      width: 400,
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/popups/option-box.hbs`;
  }

  /** @override */
  getData() {
    return { inputs: this.inputs };
  }

  submit() {
    this.render(true);
  }

  async close(options = {}) {
    Hooks.off('targetToken', this.targetChangedHookID);
    this.resolve({ cancel: true });
    return super.close();
  }

  _hasTarget() {
    return !!this.currentTarget;
  }

  _onChangeTargetDefense(event) {
    this.currentTargetDefenseType = event.currentTarget.value;
    this._updateDifficulty(getTargetDifficulty(this.currentTargetDefenseType, this.currentGroupDefenseType));
  }

  _onChangeGroupDefense(event) {
    this.currentGroupDefenseType = event.currentTarget.value;
    this._updateDifficulty(getTargetDifficulty(this.currentTargetDefenseType, this.currentGroupDefenseType));
  }

  _onCancel() {
    //this.resolve({cancel: true});
    this.close();
  }

  _onOK() {
    this.resolve({
      modifier: this.element.find('#dialog_box').val(),
      difficulty: this.element.find('#difficulty_box').val(),
      strain: this.element.find('#strain_box').val(),
      karma: this.element.find('#karma_box').val(),
      devotion: this.element.find('#devotion_box').val(),
      devotionDie: this.element.find('#devotionDie_box').val(),
    });
    this.close();
  }

  _onKeyDown(event) {
    // close dialog
    if (event.key === 'Escape') {
      event.preventDefault();
      event.stopPropagation();
      return this.close();
    }
    // confirm default choice
    if (event.key === 'Enter') {
      event.preventDefault();
      event.stopPropagation();
      this.onOK();
    }
  }

  _onTargetChanged() {
    this.currentTarget = game.user.targets.first()?.actor;
    this.currentTargets = [...game.user.targets.map((t) => t.actor)];
    this.numTargets = game.user.targets.size;
    this._updateDifficulty(getTargetDifficulty(this.currentTargetDefenseType, this.currentGroupDefenseType));
  }

  _updateDifficulty(newDifficulty) {
    this.element.find(this.difficultyBoxElementID).val(newDifficulty);
  }
}
