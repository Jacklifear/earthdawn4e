export default class earthdawn4eItemSheet extends ItemSheet {
  // talent sheet
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 500,
      height: 500,
      //the "earthdawn", "sheet", "talent" is something to point the stylesheet to for identification
      classes: [
        'earthdawn',
        'sheet',
        'discipline',
        'talent',
        'weapon',
        'skill',
        'spell',
        'attacks',
        'armor',
        'threadItem',
        'equipment',
        'namegiver',
        'shield',
        'thread',
        'knack',
        'mask',
        'devotion',
      ],
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
  }

  get template() {
    // return `systems/earthdawn4e/templates/items/sheets/${this.item.data.type}-sheet.hbs`;
    // V10 changes
    return `systems/earthdawn4e/templates/items/sheets/${this.item.type}-sheet.hbs`;
		// change End
  }

  //HTML enrich
  async _enableHTMLEnrichment() {
    let enrichment = {};
    enrichment["system.description"] = await TextEditor.enrichHTML(this.item.system.description, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionThreadItem"] = await TextEditor.enrichHTML(this.item.system.descriptionThreadItem, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionGameInfo"] = await TextEditor.enrichHTML(this.item.system.descriptionGameInfo, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionNovice"] = await TextEditor.enrichHTML(this.item.system.descriptionNovice, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionJourneyman"] = await TextEditor.enrichHTML(this.item.system.descriptionJourneyman, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionWarden"] = await TextEditor.enrichHTML(this.item.system.descriptionWarden, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionMaster"] = await TextEditor.enrichHTML(this.item.system.descriptionMaster, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle1"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle1, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle2"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle2, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle3"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle3, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle4"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle4, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle5"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle5, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle6"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle6, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle7"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle7, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle8"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle8, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle9"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle9, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle10"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle10, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle11"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle11, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle12"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle12, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle13"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle13, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle14"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle14, {async: true, secrets: this.item.isOwner});
    enrichment["system.descriptionCircle15"] = await TextEditor.enrichHTML(this.item.system.descriptionCircle15, {async: true, secrets: this.item.isOwner});
    enrichment["system.powers"] = await TextEditor.enrichHTML(this.item.system.powers, {async: true, secrets: this.item.isOwner});
    enrichment["system.racialAbilites"] = await TextEditor.enrichHTML(this.item.system.racialAbilites, {async: true, secrets: this.item.isOwner});
    return expandObject(enrichment);
  }

  async getData() {
    const systemData = super.getData();
    systemData.parentTalents = this.item.isOwned ? this.item.parent.items.filter((item) => item.type === 'talent') : [];
    systemData.isGM = game.users.current.isGM;
    systemData.enrichment =  await this._enableHTMLEnrichment();
    console.log('[EARTHDAWN] Item data: ', systemData);
    return systemData;
  }
  
  activateListeners(html) {
    super.activateListeners(html);
    html.find('.immediate-update-threads').on('input', () => {
      if (this.item.parent instanceof Actor){
        ui.notifications.error(game.i18n.localize('earthdawn.t.threadDontAdjust'))
        return
      }
      let newthreads = document.getElementsByName('system.numberthreads')[0].value;
      this.item.update({ 'system.numberthreads': newthreads }, {render: false});
    });

    html.find('.effect-add').click(() => {
      if (this.item.parent instanceof Actor){
        ui.notifications.error(game.i18n.localize('earthdawn.e.effectsDontChange'))
        return
      }
      let itemNumber = this.item.effects.size;
      let itemData = {label: `New Effect ` + itemNumber,
                      icon: "systems/earthdawn4e/assets/effect.png",
                      duration: {rounds: 1},
                      origin: this.item.id
                    }

      this.item.createEmbeddedDocuments("ActiveEffect", [itemData])
    });

    html.find('.effect-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.item.effects.get(li.data('itemId'));
      // V10 changes
      const item = this.item.effects.get(li.data('itemId'));
		  // change End
      item.sheet.render(true);
    });
    html.find('.effect-delete').click(async (ev) => {
      let li = $(ev.currentTarget).parents('.item-name'),
          itemId = li.attr('data-item-id');
      let confirmationResult = await this.confirmationBox();
      if (confirmationResult.result === false){
        return false
      }
      else{
        this.item.deleteEmbeddedDocuments('ActiveEffect', [itemId]);
      }
    });

    html.find('.change-thread-number').click(async () => {
      if (this.item.parent instanceof Actor){
        ui.notifications.error(game.i18n.localize('earthdawn.t.threadDontAdjust'))
        return
      }
      // let threadnumber = this.item.data.data.numberthreads;
      // V10 changes
      let threadnumber = this.item.system.numberthreads;
		  // change End
      const newThreads = {}
      for (let i = 1; i <= threadnumber; i ++) {
        newThreads[`rank${i}`] = { 
        keyknowledge: '',
        testknowledge: '',
        deed: '',
        effect: '',
        characteristic: '',
        value: 0,
        threadactive: false,
        hide: false, 
      };
    }

      // await this.item.update({'data.threads': null})
      // await this.item.update({ 'data.threads': newThreads })
      // V10 changes
      await this.item.update({'system.threads': null})
      await this.item.update({ 'system.threads': newThreads })
		  // change End
      });


      }
      async confirmationBox(){
        return await new Promise((resolve) => {
          new Dialog({
            title: `Confirm Delete`,
            content: `Are You Sure?
              
                  `,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.o.ok'),
                callback: (html) => {
                  resolve({
                    result: true,
                  });
                },
              },
              cancel: {
                label: game.i18n.localize('earthdawn.c.cancel'),
                callback: (html) =>{
                  resolve({
                    result: false,
                  });
                }
              }
            },
            default: 'ok',
          }).render(true);
        });
    
    
      }
      
  }

