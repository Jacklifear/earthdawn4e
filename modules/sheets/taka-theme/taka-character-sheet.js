import earthdawn4ePCSheet from '../earthdawn-4e-PC-sheet.js';

export class TakaCharacterSheet extends earthdawn4ePCSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['taka-theme'],
      width: 790,
      height: 750,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
  }

  /** @override */
  get template() {
    return `systems/earthdawn4e/templates/taka-theme/actors/character-sheet.hbs`;
  }

  /* -------------------------------------------- */

  activateListeners(html) {
    super.activateListeners(html);

    html.find('.spell-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // let itemID = li.data('itemId');
      // V10 changes pmo
      let itemID = li.data('itemId');
		  // change End
      const spell = this.actor.items.get(itemID);
      this.actor.castSpell(spell);
    });
  }
}
